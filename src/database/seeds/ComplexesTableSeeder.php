<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComplexesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start = microtime(true);
        DB::connection('dev_crm')->table('complex')
            ->leftJoin('complex_class', 'complex.complex_class_id', '=', 'complex_class.id')
            ->leftJoin('status_complex', 'complex.status_complex_id', '=', 'status_complex.id')
            ->leftJoin('metro', 'complex.metro_id', '=', 'metro.id')
            ->leftJoin('parking', 'complex.parking_id', '=', 'parking.id')
            ->leftJoin('warm', 'complex.warm_id', '=', 'warm.id')
            ->leftJoin('air', 'complex.air_id', '=', 'air.id')
            ->leftJoin('cold', 'complex.cold_id', '=', 'cold.id')
            ->leftJoin('fire_secure', 'complex.fire_secure_id', '=', 'fire_secure.id')
            ->select([ //маппим названия столбцов в старой и новой таблице (через ASs)
                'complex.name as name',
                'complex.id as id',
                'site',
                'url',
                'mc_url',
                'bc_url',
                'description',
                'architector as architect',
                'total_area',
                'human_lift',
                'cargo_lift',
                'concrete',
                'access_system',
                'builder_id',
                'complex_class.name as complex_class',
                'status_complex.name as status_complex',
                'metro.name as metro',
                'parking.name as parking',
                'warm.name as warm',
                'air.name as air',
                'cold.name as cold',
                'fire_secure.name as fire_secure'
            ])
            ->orderBy('complex.id')
            ->each(function (stdClass $v) {
            DB::table('complexes')->insert(array_filter((array) $v));
        });
        print 'Complexes filled: ' . (microtime(true) - $start) . ' sec.' . PHP_EOL;
    }
}
