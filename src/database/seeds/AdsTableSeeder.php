<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM ad ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('ads')->insert(
                [
                    'id' => $v->id,
                    'name' => $v->name,
                    'auto_paid' => $v->auto_paid
                ]
            );
        }

    }
}