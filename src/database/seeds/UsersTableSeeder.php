<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('dev_crm')
            ->table('user')
            ->orderBy('id')
            ->each(function (stdClass $v) {
                DB::table('users')->insert(
                    [
                        'id' => $v->id,
                        'name' => $v->name,
                        'description' => $v->description,
                        'email' => $v->email,
                        //password = 'secret'
                        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm'
                    ]
                );
            });
    }
}
