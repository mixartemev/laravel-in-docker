<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM currency ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('currencies')->insert(
                [
                    'name' => $v->name,
                    'rate' => $v->rate,
                    'shortname' => $v->shortname
                ]
            );
        }
    }
}
