<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListingListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM list_listing');
        foreach ($data as $k => $v) {
            DB::table('listing_list')->insert(
                [
                    'list_id' => $v->list_id,
                    'listing_id' => $v->listing_id
                ]
            );
        }
    }
}