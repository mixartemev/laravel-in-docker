<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BobjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start = microtime(true);
        DB::connection('dev_crm')->table('object')
            ->leftJoin('tax_system', 'object.tax_system_id', '=', 'tax_system.id')
            ->leftJoin('type', 'object.type_id', '=', 'type.id')
            ->leftJoin('furnish', 'object.furnish_id', '=', 'furnish.id')
            ->leftJoin('receipt_object', 'object.receipt_object_id', '=', 'receipt_object.id')
            ->select([ //маппим названия столбцов в старой и новой таблице (через ASs)
                'furnish.name as furnish',
                'type.name as type',
                'tax_system.name as tax_system',
                'receipt_object.name as receipt_object',
                'idd',
                'object.name as name',
                'object.id as id',
                'contact',
                'tower_id',
                'area',
                'corridor_coef',
                'actual_area',
                'floor',
                'ceiling',
                'comment',
                'penthouse',
                'from_builder',
                'user_id',
                'who_touched_id as updated_by',
                'leased',
                'nds',
                'with_exploit',
                'rent_price',
                'name_lessee',
                'indexing',
                'start_of_lease',
                'end_of_lease',
                'cadastral_number',
                DB::raw('IF(is_in_basket,NOW(),NULL) as deleted_at'),
            ])
            ->orderBy('object.id')
            ->each(function (stdClass $v) {
            DB::table('bobjects')->insert(array_filter((array) $v));
        });

    }
}


