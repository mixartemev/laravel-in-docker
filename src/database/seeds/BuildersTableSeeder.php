<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BuildersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start = microtime(true);
        DB::connection('dev_crm')
            ->table('builder')
            ->orderBy('id')
            ->each(function (stdClass $v) {
                DB::table('builders')->insert(
                    [
                        'name' => $v->name
                    ]
                );
            })
        ;
        print 'Builders filled: ' . (microtime(true) - $start) . ' sec.' . PHP_EOL;
    }
}
