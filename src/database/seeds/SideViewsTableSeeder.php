<?php

use Illuminate\Database\Seeder;

class SideViewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM side_view ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('side_views')->insert(
                [
                    'id' => $v->id,
                    'name' => $v->name
                ]
            );
        }

    }
}