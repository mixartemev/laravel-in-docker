<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TowersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('dev_crm')->table('tower')
            ->leftJoin('tower_type', 'tower.tower_type_id', '=', 'tower_type.id')
            ->leftJoin('tower_status', 'tower.tower_status_id', '=', 'tower_status.id')
            ->leftJoin('manage_company', 'tower.manage_company_id', '=', 'manage_company.id')
            ->select([ //маппим названия столбцов в старой и новой таблице (через ASs)
                'tower_type.name as tower_type',
                'tower_status.name as tower_status',
                'manage_company.name as manage_company',
                'tower.name as name',
                'cian_bc',
                'total_area',
                'complex_id',
                'exploit_cur_id as currency_id',
                'floors',
                'ceiling',
                'height',
                'average_floor_area',
                'number_lift',
                'description_lift',
                'year_construction',
                'street',
                'house',
                'city',
                'walk_metro',
                'exploit_office_cost',
                'exploit_apartment_cost',
                'exploit_torg_cost',
                'cadastral_value',
                'description',
                'description_lift'
            ])
            ->orderBy('tower.id')
            ->each(function (stdClass $v) {
                DB::table('towers')->insert(array_filter((array) $v, function ($e) {
                    return $e !== null;
                }));
            });
    }
}

