<?php

use Illuminate\Database\Seeder;

class InfrasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM infra ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('infras')->insert(
                [
                    'id' => $v->id,
                    'name' => $v->name
                ]
            );
        }

    }
}
