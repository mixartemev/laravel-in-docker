<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListingDealTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM listing_deal');
        foreach ($data as $k => $v) {
            DB::table('listing_deal')->insert(
                [
                    'listing_id' => $v->listing_id,
                    'deal_id' => $v->deal_id,
                    'status' => $v->status,
                    'decline_reason' => $v->decline_reason,
                ]
            );
        }
    }
}
