<?php

use Illuminate\Database\Seeder;

class LayoutElementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM layout_element ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('layout_elements')->insert(
                [
                    'id' => $v->id,
                    'name' => $v->name
                ]
            );
        }

    }
}