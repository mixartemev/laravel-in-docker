<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(DealsTableSeeder::class);
        $this->call(InfrasTableSeeder::class);

        $this->call(BuildersTableSeeder::class);
        $this->call(ComplexesTableSeeder::class);
        $this->call(TowersTableSeeder::class);
        $this->call(BobjectsTableSeeder::class);

        //rels
        $this->call(ListingsTableSeeder::class);
        $this->call(LevsTableSeeder::class);
        $this->call(ListsTableSeeder::class);

        $this->call(SideViewsTableSeeder::class);
        $this->call(SightViewsTableSeeder::class);
        $this->call(SitesTableSeeder::class);

        $this->call(ComplexesInfrasTableSeeder::class);

        $this->call(LayoutElementsTableSeeder::class);

        $this->call(ListingDealTableSeeder::class);
        $this->call(ListingLayoutElementTableSeeder::class);
        $this->call(ListingLev::class);
        $this->call(ListingListTableSeeder::class);
        $this->call(ListingSideViewTableSeeder::class);
        $this->call(ListingSightViewTableSeeder::class);
        $this->call(ListingSiteTableSeeder::class);
    }
}
