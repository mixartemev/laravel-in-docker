<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListingSiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM listing_site');
        foreach ($data as $k => $v) {
            DB::table('listing_site')->insert(
                [
                    'listing_id' => $v->listing_id,
                    'site_id' => $v->site_id
                ]
            );
        }
    }
}