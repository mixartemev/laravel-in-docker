<?php

use Illuminate\Database\Seeder;

class LevsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM layout_element_val ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('levs')->insert(
                [
                    'id' => $v->id,
                    'name' => $v->name
                ]
            );
        }

    }
}