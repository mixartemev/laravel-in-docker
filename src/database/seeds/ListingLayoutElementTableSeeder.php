<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListingLayoutElementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM listing_layout_element');
        foreach ($data as $k => $v) {
            DB::table('listing_layout_element')->insert(
                [
                    'listing_id' => $v->listing_id,
                    'layout_element_id' => $v->layout_element_id
                ]
            );
        }
    }
}