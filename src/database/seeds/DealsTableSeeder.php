<?php

use Illuminate\Database\Seeder;

class DealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM deal ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('deals')->insert(
                [
                    'id' => $v->id,
                    'status' => $v->status,
                    'amo_link' => $v->amo_link
                ]
            );
        }

    }
}