<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComplexesInfrasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM complex_infra');
        foreach ($data as $k => $v) {
            DB::table('complex_infra')->insert(
                [
                    'complex_id' => $v->complex_id,
                    'infra_id' => $v->infra_id
                ]
            );
        }
    }
}
