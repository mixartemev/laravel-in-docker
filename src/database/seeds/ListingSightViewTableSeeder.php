<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListingSightViewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM listing_view_to');
        foreach ($data as $k => $v) {
            DB::table('listing_sight_view')->insert(
                [
                    'listing_id' => $v->listing_id,
                    'sight_view_id' => $v->view_to_id
                ]
            );
        }
    }
}