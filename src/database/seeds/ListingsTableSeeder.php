<?php

use Illuminate\Database\Seeder;

class ListingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('dev_crm')->table('listing')
            ->leftJoin('listing_type', 'listing.listing_type_id', '=', 'listing_type.id')
            ->leftJoin('type', 'listing.type_id', '=', 'type.id')
            ->leftJoin('layout', 'listing.layout_id', '=', 'layout.id')
            ->leftJoin('wc_type', 'listing.wc_type_id', '=', 'wc_type.id')
            ->leftJoin('furniture_condition', 'listing.furniture_condition_id', '=', 'furniture_condition.id')
            ->leftJoin('agreement_complete', 'listing.agreement_complete_id', '=', 'agreement_complete.id')
            ->leftJoin('contract_type', 'listing.contract_type_id', '=', 'contract_type.id')
            ->select([ //маппим названия столбцов в старой и новой таблице (через ASs)
                'listing_type.name as listing_type',
                'type.name as type',
                'layout.name as layout',
                'wc_type.name as wc_type',
                'furniture_condition.name as furniture_condition',
                'agreement_complete.name as agreement_complete',
                'contract_type.name as contract_type',
                'idd',
                'listing.id as id',
                'listing.name as name',
                'listing_name',
                'description',
                'object_id as bobject_id',
                'activity',
                'area',
                'corridor_coef',
                'actual_area',
                'cost_m2',
                'cost',
                'currency_id',
                'nds',
                'usn',
                'user_id',
                'who_touched_id as updated_by',
                'operation_included',
                'electricity_included',
                'internet',
                'telephone',
                'cleaning_included',
                'parking_included',
                'included',
                'prepayment',
                'security_payment',
                'indexing',
                'vacation_rentals',
                'description_deal',
                'ak_proc',
                'ak_rub',
                'recommendations',
                'proc_recommendations',
                'rub_recommendations',
                'ad_desc',
                'youtube',
                'mc_url',
                'bc_url',
                'no_board',
                'no_site',
                'furniture_included',
                'contract_duration',
                'comment',
                'to_moderate',
                'ava_from_object',
                'contract_sign',
                'update_activity',
                DB::raw('IF(is_in_basket,NOW(),NULL) as deleted_at'),
            ])
            ->orderBy('listing.id')
            ->each(function (stdClass $v) {
                DB::table('listings')->insert(array_filter((array) $v));
            });
    }
}