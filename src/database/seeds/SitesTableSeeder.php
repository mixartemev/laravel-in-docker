<?php

use Illuminate\Database\Seeder;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM site ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('sites')->insert(
                [
                    'id' => $v->id,
                    'name' => $v->name
                ]
            );
        }

    }
}