<?php

use Illuminate\Database\Seeder;

class SightViewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM view_to ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('sight_views')->insert(
                [
                    'id' => $v->id,
                    'name' => $v->name
                ]
            );
        }

    }
}