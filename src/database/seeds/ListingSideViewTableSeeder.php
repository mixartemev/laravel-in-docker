<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListingSideViewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM listing_side_view');
        foreach ($data as $k => $v) {
            DB::table('listing_side_view')->insert(
                [
                    'listing_id' => $v->listing_id,
                    'side_view_id' => $v->side_view_id
                ]
            );
        }
    }
}