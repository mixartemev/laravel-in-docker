<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListingLev extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM listing_layout_element_val');
        foreach ($data as $k => $v) {
            DB::table('listing_lev')->insert(
                [
                    'listing_id' => $v->listing_id,
                    'lev_id' => $v->layout_element_val_id,
                    'val' => $v->val
                ]
            );
        }
    }
}