<?php

use Illuminate\Database\Seeder;

class ListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::connection('dev_crm')->select('select * FROM list ORDER BY id');
        foreach ($data as $k => $v) {
            DB::table('lists')->insert(
                [
                    'id' => $v->id,
                    'user_id' => $v->user_id,
                    'name' => $v->name,
                ]
            );
        }

    }
}