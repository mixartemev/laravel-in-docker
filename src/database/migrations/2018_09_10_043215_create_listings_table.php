<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('idd')->unique()->comment('Идентификатор');
            $table->string('name')->nullable()->comment('Название листинга');
		    $table->string('listing_name')->default('')->comment('Имя помещения');
		    $table->text('description', 1023)->nullable()->comment('Описание');
            $table->unsignedInteger('bobject_id')->comment('Объект');
		    $table->boolean('activity')->default(0)->comment('Активность');
            $table->enum('listing_type', ['Аренда', 'Продажа'])->comment('Тип листингаи');
            $table->enum('type', ['Апартамент', 'Офис', 'Торговая недвижимость', 'Машиноместо'])->comment('Вид недвижимости');
		    $table->decimal('area', 7, 2)->nullable()->comment('Общая площадь (кв.м.)');
		    $table->decimal('corridor_coef', 3, 1)->nullable()->comment('Коридорный коэф (%)');
		    $table->decimal('actual_area', 7, 2)->nullable()->comment('Фактическая площадь кв.м.');
		    $table->decimal('cost_m2', 13, 2)->comment('Стоимость квадратного метра');
		    $table->decimal('cost', 13, 2)->comment('Стоимость');
		    $table->unsignedTinyInteger('currency_id')->comment('Валюта');
            $table->enum('layout', ['Открытая', 'Смешанная', 'Кабинетная'])->comment('Планировка');
            $table->enum('wc_type', ['собственный', 'на этаже', 'собственный с душевой кабиной'])->comment('Тип санузла');
            $table->enum('furniture_condition', ['есть'])->comment('Состояние мебели');
		    $table->boolean('nds')->nullable()->comment('НДС');
		    $table->boolean('usn')->nullable()->comment('УСН');
		    $table->unsignedInteger('user_id')->default('1')->comment('Ответственный');
            $table->unsignedInteger('updated_by')->nullable()->comment('Кто последний трогал');
            $table->unsignedInteger('created_by')->nullable()->comment('Кто создал');
		    $table->boolean('operation_included')->nullable()->comment('Эксплуатация включена');
		    $table->boolean('electricity_included')->nullable()->comment('Электричество включено');
		    $table->boolean('internet')->nullable()->comment('Интернет включен');
		    $table->boolean('telephone')->nullable()->comment('Телеофония включена');
		    $table->boolean('cleaning_included')->nullable()->comment('Клининг включен');
		    $table->boolean('parking_included')->nullable()->comment('Паркинг включен');
		    $table->string('included')->nullable()->comment('Дополнительно включено');
		    $table->unsignedSmallInteger('prepayment')->nullable()->comment('Предоплата (мес)');
		    $table->decimal('security_payment', 7, 2)->nullable()->comment('Обеспечительный платеж');
		    $table->string('indexing')->nullable()->comment('Индексация %');
		    $table->unsignedSmallInteger('vacation_rentals')->nullable()->comment('Арендные каникулы (мес)');
            $table->enum('agreement_complete', ['Физическое лицо', 'Юридическое лицо'])->comment('Договор оформлен');
            $table->enum('contract_type', ['Прямой', 'Субаренда', 'Переуступка', 'ДКП', 'ПДКП', 'Договор инвестирования', 'ДДУ'])->comment('Тип договора');
            $table->enum('deal_sheme', ['Прямая', 'Схема'])->comment('Схема сделки');
		    $table->text('description_deal')->nullable()->comment('Описание сделки');
		    $table->decimal('ak_proc', 5, 2)->default('0.00')->comment('АК %');
		    $table->decimal('ak_rub', 13, 2)->nullable()->comment('АК руб');
		    $table->string('recommendations')->nullable()->comment('Рекомендодатель');
		    $table->unsignedSmallInteger('proc_recommendations')->nullable()->comment('% рекомендодателю');
		    $table->decimal('rub_recommendations', 13, 2)->nullable()->comment('руб. рекомендодталю');
		    $table->string('ad_desc', 1023)->nullable()->comment('Описание для досок');
		    $table->string('youtube', 255)->nullable()->comment('Ссылка на youtube');
		    $table->string('mc_url')->nullable()->comment('Ссылка на MCity');
		    $table->string('bc_url')->nullable()->comment('Ссылка на БашниСити');
		    $table->boolean('no_board')->default(0)->comment('Не выгружать на доски');
		    $table->boolean('no_site')->default(0)->comment('Не выгружать на сайт');
		    $table->boolean('furniture_included')->default(0)->comment('Мебель включена');
		    $table->boolean('contract_duration')->nullable()->comment('Срок договора аренды');
		    $table->string('comment', 4095)->default('')->comment('Коментарий');
		    $table->boolean('to_moderate')->default(0)->comment('На проверку');
		    $table->boolean('ava_from_object')->default(0)->comment('Ава из объекта');
		    $table->boolean('contract_sign')->default(0)->comment('Контракт подписан');
            $table->timestamp('deleted_at')->nullable()->comment('В корзине');
		    $table->unsignedInteger('update_activity')->nullable()->comment('Дата обновления активности');

            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('updated_by')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('bobject_id')->references('id')->on('bobjects')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
