<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('owner_id')->comment('Владелец фотки');
            $table->timestamp('deleted_at')->nullable()->comment('Удалена');
            $table->enum('owner_type', ['listings', 'bobjects', 'towers', 'complexes'])->comment('Тип владельца');
            $table->enum('type', ['general', 'view', 'plan'])->comment('Тип фотки');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
