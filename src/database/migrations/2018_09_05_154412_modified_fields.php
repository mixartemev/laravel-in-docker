<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifiedFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('modified_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('field_name', '255');
            $table->string('new_value', '255');
            $table->unsignedInteger('history_action_id')->index();

            $table->foreign('history_action_id')->references('id')->on('history_actions')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modified_fields');
    }
}
