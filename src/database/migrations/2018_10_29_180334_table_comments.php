<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class TableComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $comments = [
            'ads' => 'Застройщики',
            'builders' => 'Застройщики',
            'complexes' => 'Комплексы',
            'currencies' => 'Валюты',
            'towers' => 'Башни',
            'bobjects' => 'Объекты',
            'listings' => 'Листинги',
            'deals' => 'Листинги',
            'history_actions' => 'Лог действий',
            'infras' => 'Инфраструктуры',
            'layout_elements' => 'Элементы планировки',
            'levs' => 'Количественные элементы планировки',
            'side_views' => 'Виды на сторону света',
            'sight_views' => 'Виды на объект',
            'sites' => 'Сайты',
            'users' => 'Пользователи',
        ];

        $template = 'is_postgres' ? "COMMENT ON TABLE %s IS '%s'" : "ALTER TABLE %s COMMENT '%s'";

        foreach ($comments as $t => $c) {
            DB::statement(sprintf($template, $t, $c));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
