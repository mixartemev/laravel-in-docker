<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('complexes', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 255)->unique()->comment('Название');
            $table->string('site', 255)->nullable()->unique()->comment('Официальный сайт');
            $table->string('url', 255)->nullable()->unique();
            $table->string('mc_url', 255)->nullable()->unique()->comment('url на mcity');
            $table->string('bc_url', 255)->nullable()->unique()->comment('url на башнях');
            $table->string('description', 4095)->default('')->comment('Описание');
            $table->string('architect', 255)->default('')->comment('Архитектор');
            $table->unsignedInteger('total_area')->default(0)->comment('Общая площадь');
            $table->unsignedTinyInteger('human_lift')->nullable()->comment('Количество пассажирских лифтов');
            $table->unsignedTinyInteger('cargo_lift')->nullable()->comment('Количество грузовых лифтов');
            $table->enum('complex_class', ['A'])->default('A')->comment('Класс комплекса');
            $table->enum('status_complex', ['Действующее', 'Строящееся', 'Проект'])->default('Действующее')
                ->comment('Статус комплекса');
            $table->enum('metro', ['Деловой центр', 'Выставочная', 'Международная'])->default('Деловой центр')
                ->comment('Метро');
            $table->enum('parking', ['Подземная', 'Наземная'])->default('Подземная')->comment('Парковка');
            $table->enum('warm', ['Автономное', 'Центральное', 'Нет'])->default('Центральное')->comment('Отопление');
            $table->enum('air', ['Приточная', 'Нет'])->default('Нет')->comment('Вентиляция');
            $table->enum('cold', ['Центральное', 'Местное'])->default('Центральное')->comment('Кондиционирование');
            $table->enum('fire_secure', ['Спринклерная'])->default('Спринклерная')->comment('Система пожаротушения');
            $table->boolean('concrete')->default(false)->comment('Бетон');
            $table->boolean('access_system')->default(true)->comment('Пропускная система');
            $table->unsignedTinyInteger('builder_id')->nullable()->index()->comment('Застройщик');
            $table->unsignedInteger('updated_by')->nullable()->comment('Кто последний трогал');
            $table->unsignedInteger('created_by')->nullable()->comment('Кто создал');

            $table->foreign('updated_by')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('builder_id')->references('id')->on('builders')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complexes');
    }
}
