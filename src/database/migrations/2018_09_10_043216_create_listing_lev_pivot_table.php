<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingLevPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('listing_lev', function (Blueprint $table) {
            $table->unsignedInteger('listing_id')->index();
            $table->unsignedTinyInteger('lev_id')->index();
            $table->unsignedSmallInteger('val')->default('1');

            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('lev_id')->references('id')->on('levs')->onDelete('cascade');

            $table->primary(['listing_id', 'lev_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_lev');
    }
}
