<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingLayoutElementPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('listing_layout_element', function (Blueprint $table) {
            $table->unsignedInteger('listing_id')->index();
            $table->unsignedTinyInteger('layout_element_id')->index();

            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('layout_element_id')->references('id')->on('layout_elements')->onDelete('cascade');

            $table->primary(['listing_id', 'layout_element_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_layout_element');
    }
}
