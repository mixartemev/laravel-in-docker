<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplexInfraPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('complex_infra', function (Blueprint $table) {
            $table->unsignedTinyInteger('complex_id')->index();
            $table->foreign('complex_id')->references('id')->on('complexes')->onDelete('cascade');
            $table->unsignedTinyInteger('infra_id')->index();
            $table->foreign('infra_id')->references('id')->on('infras')->onDelete('cascade');
            $table->primary(['complex_id', 'infra_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('complex_infra');
    }
}
