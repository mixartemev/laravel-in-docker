<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('towers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->comment('Название');
            $table->unsignedInteger('cian_bc')->nullable()->comment('Циан БЦ ID');
            $table->unsignedInteger('total_area')->comment('Общая площадь (кв.м.)');
            $table->unsignedTinyInteger('complex_id')->comment('Комплекс');
            $table->unsignedTinyInteger('currency_id')->comment('Валюта');
            $table->enum('tower_type', ['Офисное здание', 'Офисно-жилой комплекс', 'Жилой комплекс'])
                ->comment('Тип здания');
            $table->enum('tower_status', ['Действующие', 'Строящиеся'])->comment('Статус здания');
            $table->unsignedSmallInteger('floors')->comment('Этажность');
            $table->decimal('ceiling', 3, 1)->comment('Высота потолков');
            $table->unsignedSmallInteger('height')->nullable()->comment('Высотность');
            $table->unsignedInteger('average_floor_area')->nullable()->comment('Средняя площадь этажа');
            $table->smallInteger('number_lift')->nullable()->comment('Кол-во лифтов');
            $table->string('description_lift', 255)->nullable()->comment('Описание лифтов');
            $table->unsignedInteger('year_construction')->comment('Год постройки');
            $table->string('street', 255)->comment('Улица');
            $table->string('house', 255)->comment('Дом');
            $table->string('city', 255)->comment('Город');
            $table->unsignedTinyInteger('walk_metro')->comment('Пешком от метро');
            $table->enum('manage_company', ['ВБ Сервис', 'Federation Group', 'Capital Group',
                'Empire management', 'Mercury tower management', 'ООО "Капэкс"', 'Enka', 'Галс-Девелопмент',
                'УК ПЛАЗА МЕНЕДЖМЕНТ', 'Р7 Групп'])->comment('Управляющая компания');
            $table->decimal('exploit_office_cost', 7, 2)->nullable()
                ->comment('Эксплуатация офисы (за кв.м. в год)');
            $table->decimal('exploit_apartment_cost', 7, 2)->nullable()
                ->comment('Эксплуатация Апартаменты  (за кв.м. в год)');
            $table->decimal('exploit_torg_cost', 7, 2)->nullable()
                ->comment('Эксплуатация торговые помещения  (за кв.м. в год)');
            $table->unsignedInteger('updated_by')->nullable()->comment('Кто последний трогал');
            $table->unsignedInteger('created_by')->nullable()->comment('Кто создал');

            $table->foreign('updated_by')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->decimal('cadastral_value', 13, 2)->nullable()->comment('Кадастровая стоимость');
            $table->string('description', 255)->nullable()->comment('Описание');

            $table->foreign('complex_id')->references('id')->on('complexes')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('towers');
    }
}
