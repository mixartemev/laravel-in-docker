<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBobjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bobjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idd', 255)->comment('Идентификатор');

            $table->string('name', 255)->nullable()->comment('Название');
		    $table->unsignedInteger('contact')->nullable()->comment('Контакт');
		    $table->unsignedInteger('tower_id')->comment('Здание');
            $table->enum('type', ['Апартамент', 'Офис', 'Торговая недвижимость', 'Машиноместо'])->comment('Вид недвижимости');
		    $table->decimal('area', 7, 2)->comment('Общая площадь (кв.м.)');
		    $table->decimal('corridor_coef', 3, 1)->nullable()->comment('Коридорный коэф (%)');
		    $table->decimal('actual_area', 7, 2)->nullable()->comment('Фактическая площадь кв.м.');
		    $table->smallInteger('floor')->default(0)->comment('Этаж');
		    $table->decimal('ceiling', 4, 1)->nullable()->comment('Высота потолка');
            $table->enum('furnish', ['С отделкой', 'Shell&Core', 'White box'])->comment('Отделка');
		    $table->string('comment', 4095)->nullable()->comment('Комментарий');
            $table->enum('receipt_object', ['Сайт', 'Объявление в интернете', 'Рекомендация', 'Обзвон. базы', 'Руководство', 'Реклама'])->comment('Источник поступления объекта');
		    $table->boolean('penthouse')->nullable()->comment('Пентхаус');
		    $table->boolean('from_builder')->nullable()->comment('От застройщика');
		    $table->unsignedInteger('user_id')->default('1')->comment('Ответсвенный');
            $table->unsignedInteger('updated_by')->nullable()->comment('Кто последний трогал');
            $table->unsignedInteger('created_by')->nullable()->comment('Кто создал');
		    $table->boolean('leased')->nullable()->comment('Сдано в аренду');
		    $table->boolean('with_exploit')->nullable()->comment('Включает эксплуатацию');
		    $table->decimal('rent_price', 9, 2)->nullable()->comment('Стоимость аренды (руб кв.м. в год)');
		    $table->string('name_lessee', 255)->nullable()->comment('Наименование арендатора');
            $table->enum('tax_system', ['НДС', 'УСН'])->nullable()->comment('Система налогооблажения арендодателя');
		    $table->string('indexing', 255)->nullable()->comment('Индексация %');
		    $table->date('start_of_lease')->nullable()->comment('Дата начала договора аренды');
		    $table->date('end_of_lease')->nullable()->comment('Дата окончания договора аренды');
		    $table->string('cadastral_number')->nullable()->comment('Кадастровый номер');
		    $table->timestamp('deleted_at')->nullable()->comment('В корзине');

            $table->foreign('tower_id')->references('id')->on('towers')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('updated_by')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
