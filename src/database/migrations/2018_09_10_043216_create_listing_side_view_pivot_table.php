<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingSideViewPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('listing_side_view', function (Blueprint $table) {
            $table->unsignedInteger('listing_id')->index();
            $table->unsignedTinyInteger('side_view_id')->index();

            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('side_view_id')->references('id')->on('side_views')->onDelete('cascade');

            $table->primary(['listing_id', 'side_view_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_side_view');
    }
}
